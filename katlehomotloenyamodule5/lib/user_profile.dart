// ignore_for_file: avoid_print

import 'dart:developer';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'dashboard.dart';
import 'main.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'package:path/path.dart' as path;

String tempPass = "", confPass = "";

class UserProfile extends StatefulWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  State<UserProfile> createState() => UserProfileState();
}

class UserProfileState extends State<UserProfile> {
  ImagePicker picker = ImagePicker();
  File? imgFile = File("");

  @override
  void initState() {
    late String fn = 'No Image';
    if (ud.image != null) {
      fn = path.basename(ud.image!.path);
    }
    String fName = '${ud.phone}_$fn';
    getImage(fName);
    super.initState();
  }

  void uploadImage() async {
    String fn = 'No Image';
    if (ud.image != null) {
      fn = path.basename(ud.image!.path);
    }

    Reference? PPRef = profilePics.child('Profile Pictures/${ud.phone}_$fn');
    try {
      print('file wait');
      if (ud.image != null) {
        await PPRef.putFile(ud.image!);
        print('file done');
      }
    } on FirebaseException catch (e) {
      print(e);
    }
    setState(() {
      String fName = '${ud.phone}_$fn';
      getImage(fName);
    });
  }

  void registerUser() async {
    print('ref done renaming');
    //changeFileNameOnlySync(ud.image!, ud.phone);
    /*  String ext = path.extension(ud.image!.path);
      String dir = path.dirname(ud.image!.path);
      String newName = path.join(dir, '${ud.phone}$ext');
      ud.image!.renameSync(newName); */
    //print('renamed ${basename(ud.image!.path)}');
    late String fn = 'No Image';
    if (ud.image != null) {
      fn = path.basename(ud.image!.path);
    }

    Reference? PPRef = profilePics.child('Profile Pictures/${ud.phone}_$fn');
    setState(() {
      //final PP = PPRef.child(ud.image!.path);

      FirebaseFirestore.instance
          .collection('Users')
          .doc(ud.email)
          .set({
            'Full Name': ud.name,
            'Email': ud.email,
            'Phone': ud.phone,
            'Address': ud.address,
            'Password': ud.password,
            'Image': '${ud.phone}_$fn',
            'Link': ud.link
          })
          .then((value) => log('User added ${ud.name}'))
          .catchError(
              (onError) => log('Error adding user ${ud.name}: $onError'));
    });
    try {
      print('file wait');
      if (ud.image != null) {
        await PPRef.putFile(ud.image!);
        print('file done');
      }
    } on FirebaseException catch (e) {
      print(e);
    }
  }

  void getImage(String fName) async {
    final pp =
        FirebaseStorage.instance.ref().child('Profile Pictures').child(fName);

    ud.link = await pp.getDownloadURL();
    if (ud.link == '') {
      ud.link =
          'https://support.hubstaff.com/wp-content/uploads/2019/08/good-pic.png';
    }
    FirebaseFirestore.instance
        .collection('Users')
        .doc(ud.email)
        .update({'Link': ud.link, 'Image': fName});
  }

  // ignore: non_constant_identifier_names
  Future<bool> user_exists(String email) {
    return FirebaseFirestore.instance
        .collection('Users')
        .doc(email)
        .get()
        .then((value) {
      return value.exists;
    });
  }

  Future getIma() async {
    await picker.pickImage(source: ImageSource.gallery).then((value) {
      setState(() {
        ud.image = File(value!.path);
      });
    });
    setState(() {
      uploadImage();
    });
  }

  void editProfile() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(
                20.0,
              ),
            ),
          ),
          contentPadding: const EdgeInsets.only(
            top: 10.0,
          ),
          content: SizedBox(
            height: 700,
            width: 800,
            child: SingleChildScrollView(
              child: SafeArea(
                minimum: const EdgeInsets.only(top: 0),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: const Alignment(-0.93, 0.9),
                      child: FloatingActionButton(
                          heroTag: "Back",
                          onPressed: () =>
                              {context, Navigator.of(context).pop()},
                          tooltip: 'Back',
                          child: const Icon(Icons.arrow_back)),
                    ),
                    GestureDetector(
                      onTap: () {
                        getIma();
                      },
                      child: CircleAvatar(
                          backgroundColor: Colors.orange[200],
                          radius: 50,
                          backgroundImage: ud.image != null
                              ? FileImage(ud.image!)
                              : NetworkImage(ud.link) as ImageProvider),
                    ),
                    const SizedBox(
                      height: 20,
                      width: 50,
                      child: Divider(
                        color: Colors.grey,
                      ),
                    ),
                    Card(
                      child: Row(
                        children: [
                          const InkWell(
                            splashColor: Colors.grey,
                            child: SizedBox(
                              width: 70,
                              height: 50,
                              child: Icon(Icons.person),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            width: 200,
                            child: TextFormField(
                              initialValue: ud.name,
                              decoration: const InputDecoration(
                                  labelText: "Enter Full Name"),
                              onChanged: (name) {
                                FirebaseFirestore.instance
                                    .collection('Users')
                                    .doc(ud.email)
                                    .update({'Full Name': name});
                                ud.setName(name);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                      width: 300,
                      child: Divider(
                        color: Colors.grey,
                      ),
                    ),
                    Card(
                      child: Row(
                        children: [
                          const InkWell(
                            splashColor: Colors.grey,
                            child: SizedBox(
                              width: 70,
                              height: 50,
                              child: Icon(Icons.email),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            width: 200,
                            child: TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              initialValue: ud.email,
                              decoration: const InputDecoration(
                                  labelText: "Enter Email"),
                              onFieldSubmitted: (email) {
                                late Map<String, dynamic> results;
                                FirebaseFirestore.instance
                                    .collection('Users')
                                    .doc(ud.email)
                                    .get()
                                    .then((DocumentSnapshot ds) {
                                  if (ds.exists) {
                                    results = ds.data() as Map<String, dynamic>;
                                    ud.image = results['Image'];
                                    ud.address = results['Address'];
                                    ud.email = results['Email'];
                                    ud.name = results['Full Name'];
                                    ud.password = results['Password'];
                                    ud.phone = results['Phone'];
                                    ud.link = results['Link'];
                                  }
                                });
                                FirebaseFirestore.instance
                                    .collection('Users')
                                    .doc(ud.email)
                                    .delete();
                                ud.setEmail(email);
                                user_exists(email).then((value) {
                                  if (value != true) {
                                    registerUser();
                                  }
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                      width: 300,
                      child: Divider(
                        color: Colors.grey,
                      ),
                    ),
                    Card(
                      child: Row(
                        children: [
                          const InkWell(
                            splashColor: Colors.grey,
                            child: SizedBox(
                              width: 70,
                              height: 50,
                              child: Icon(Icons.phone),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            width: 200,
                            child: TextFormField(
                              keyboardType: TextInputType.phone,
                              initialValue: ud.phone,
                              decoration: const InputDecoration(
                                  labelText: "Enter Phone Number"),
                              onChanged: (phone) {
                                FirebaseFirestore.instance
                                    .collection('Users')
                                    .doc(ud.email)
                                    .update({'Phone': phone});
                                ud.setPhone(phone);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                      width: 300,
                      child: Divider(
                        color: Colors.grey,
                      ),
                    ),
                    Card(
                      child: Row(
                        children: [
                          const InkWell(
                            splashColor: Colors.grey,
                            child: SizedBox(
                              width: 70,
                              height: 100,
                              child: Icon(Icons.location_city),
                            ),
                          ),
                          SizedBox(
                            height: 80,
                            width: 200,
                            child: TextFormField(
                              keyboardType: TextInputType.multiline,
                              maxLines: 10,
                              initialValue: ud.address,
                              decoration: const InputDecoration(
                                  labelText: "Enter Address"),
                              onChanged: (address) {
                                FirebaseFirestore.instance
                                    .collection('Users')
                                    .doc(ud.email)
                                    .update({'Address': address});
                                ud.setAddress(address);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                      width: 300,
                      child: Divider(
                        color: Colors.grey,
                      ),
                    ),
                    Card(
                      child: Row(
                        children: [
                          const InkWell(
                            splashColor: Colors.grey,
                            child: SizedBox(
                              width: 70,
                              height: 50,
                              child: Icon(Icons.password),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            width: 90,
                            child: TextFormField(
                              initialValue: ud.password,
                              decoration: const InputDecoration(
                                  labelText: "Enter Password"),
                              onChanged: (password) {
                                tempPass = password;
                              },
                              obscureText: true,
                            ),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          SizedBox(
                            height: 50,
                            width: 100,
                            child: TextFormField(
                              initialValue: ud.password,
                              decoration: const InputDecoration(
                                  labelText: "Confirm Password"),
                              onChanged: (password) {
                                confPass = password;
                              },
                              obscureText: true,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                      width: 300,
                      child: Divider(
                        color: Colors.grey,
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        if (tempPass != "" && confPass != "") {
                          if (tempPass.compareTo(confPass) == 0) {
                            FirebaseFirestore.instance
                                .collection('Users')
                                .doc(ud.email)
                                .update({'Password': tempPass});
                            ud.password = tempPass;
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const MyDashboard()));
                            _showToast(context, 'Details Saved');
                          } else {
                            _showToast(context, 'Passwords do not match');
                          }
                        } else {
                          _showToast(context,
                              'Please re-enter password to save details');
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        primary: Colors.orange,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 50, vertical: 20),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0)),
                        textStyle: const TextStyle(color: Colors.white),
                      ),
                      child: const Text(
                        "SAVE CHANGES",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void _showToast(BuildContext context, String msg) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text(msg),
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: NotOut(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SingleChildScrollView(
                child: SafeArea(
                  minimum: const EdgeInsets.only(top: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                          backgroundColor: Colors.orange[200],
                          radius: 50,
                          backgroundImage: ud.image != null
                              ? FileImage(
                                  ud.image!,
                                )
                              : NetworkImage(ud.link) as ImageProvider),
                      Text(
                        ud.name,
                        style: const TextStyle(
                            fontSize: 35.0,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Pacifico'),
                      ),
                      const SizedBox(
                        height: 20,
                        width: 300,
                        child: Divider(
                          color: Colors.grey,
                        ),
                      ),
                      Card(
                        child: Row(
                          children: [
                            const InkWell(
                              splashColor: Colors.grey,
                              child: SizedBox(
                                width: 70,
                                height: 50,
                                child: Icon(Icons.email),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(ud.email)
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                        width: 300,
                        child: Divider(
                          color: Colors.grey,
                        ),
                      ),
                      Card(
                        child: Row(
                          children: [
                            const InkWell(
                              splashColor: Colors.grey,
                              child: SizedBox(
                                width: 70,
                                height: 50,
                                child: Icon(Icons.phone),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(ud.phone)
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                        width: 300,
                        child: Divider(
                          color: Colors.grey,
                        ),
                      ),
                      Card(
                        child: Row(
                          children: [
                            const InkWell(
                              splashColor: Colors.grey,
                              child: SizedBox(
                                width: 70,
                                height: 100,
                                child: Icon(Icons.location_city),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: Container(
                                padding: const EdgeInsets.only(right: 13.0),
                                child: Text(
                                  ud.address,
                                  overflow: TextOverflow.fade,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                        width: 300,
                        child: Divider(
                          color: Colors.grey,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 5),
                            child: ElevatedButton(
                              onPressed: () {
                                FirebaseFirestore.instance
                                    .collection('Users')
                                    .doc(ud.email)
                                    .delete()
                                    .then((value) => Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const LoginPage()),
                                        ))
                                    .onError((error, stackTrace) => print(
                                        'Error: $error. Stack: $stackTrace'));
                              },
                              style: ElevatedButton.styleFrom(
                                primary: Colors.orange,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 20),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(80.0)),
                                textStyle: const TextStyle(color: Colors.white),
                              ),
                              child: const Text(
                                "Delete Account",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 110,
                          ),
                          Align(
                            alignment: const Alignment(0.90, 0.0),
                            child: FloatingActionButton(
                              heroTag: "Edit",
                              onPressed: () => {editProfile()},
                              tooltip: 'Edit',
                              child: const Icon(Icons.edit),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ).build(context),
    );
  }
}

/* class EditProfile extends StatefulWidget {
  const EditProfile({
    Key? key,
  }) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  @override
  Widget build(BuildContext context) {
    return 
  }

  
} */
