// ignore_for_file: avoid_print

import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;

import 'dashboard.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
// ignore: import_of_legacy_library_into_null_safe
import 'package:splashscreen/splashscreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:path/path.dart';

String tempEmail = "";
String tempPass = "";
final profilePics = FirebaseStorage.instance.ref();
UserDetails ud = UserDetails();
void main() async {
  print('start');
  WidgetsFlutterBinding.ensureInitialized();
  print('widget done');
  await Firebase.initializeApp();
  print('firebase done');
  runApp(const MyApp());
}

class UserDetails {
  File? image;
  String link =
      'https://support.hubstaff.com/wp-content/uploads/2019/08/good-pic.png';
  String name = '';
  String email = '';
  String phone = '';
  String address = '';
  String password = '';

  void setImage(File? image) {
    this.image = image;
  }

  File getImage() {
    return image!;
  }

  void setName(String name) {
    this.name = name;
  }

  String getName() {
    return name;
  }

  String getEmail() {
    return email;
  }

  void setEmail(String email) {
    this.email = email;
  }

  String getPhone() {
    return phone;
  }

  void setPhone(String phone) {
    this.phone = phone;
  }

  String getPassword() {
    return password;
  }

  void setPassword(String password) {
    this.password = password;
  }

  String getAddress() {
    return address;
  }

  void setAddress(String address) {
    this.address = address;
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.orange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(
        seconds: 8,
        navigateAfterSeconds: const LoginPage(),
        title: const Text(
          'Welcome. To IT Varsity',
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20.0, color: Colors.black),
        ),
        image: Image.asset(
            'assets/ITv-Logo.png'), //'assets/icons/ts_logo_white_bg.png'),
        photoSize: 100.0,
        backgroundColor: Colors.orange[200],
        styleTextUnderTheLoader: const TextStyle(),
        loaderColor: Colors.black,
      ),
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  FirebaseAuth authUser = FirebaseAuth.instance;

  @override
  void initState() {
    super.initState();
    ud = UserDetails();
  }

  void getImage(String fName) async {
    final pp =
        FirebaseStorage.instance.ref().child('Profile Pictures').child(fName);

    ud.link = await pp.getDownloadURL();
    if (ud.link == '') {
      ud.link =
          'https://support.hubstaff.com/wp-content/uploads/2019/08/good-pic.png';
    }
    print(ud.link);
  }

  void login(String email, String password, BuildContext context) {
    print('start login');
    late Map<String, dynamic> results;
    if (email != '' && password != '') {
      FirebaseFirestore.instance
          .collection('Users')
          .doc(email)
          .get()
          .then((DocumentSnapshot ds) {
        if (ds.exists) {
          results = ds.data() as Map<String, dynamic>;
          if (password == results['Password']) {
            if (ud.image != null) {
              getImage(results['Image']);
            }
            ud.address = results['Address'];
            ud.email = results['Email'];
            ud.name = results['Full Name'];
            ud.password = results['Password'];
            ud.phone = results['Phone'];
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => const MyDashboard()),
            );
            tempPass = "";
            tempEmail = "";
            _showToast(context, 'Logged IN.');
          } else {
            _showToast(context, 'Incorrect details. Please try again');
          }
        } else {
          _showToast(context, 'User does not exist. Please register');
        }
      });
    } else {
      _showToast(context, 'User details needed');
    }

    // print('usersDetails: $uu');

    /* FutureBuilder<DocumentSnapshot>(builder:
        (BuildContext context, AsyncSnapshot<DocumentSnapshot>? users) {
      print('Builders');
      if (email != "" && password != "") {
        if (users!.hasError) {
          print('Error with snap');
        }
        if (users.connectionState == ConnectionState.done) {
          print('connection');
          Map<String, dynamic> results =
              users.data!.data() as Map<String, dynamic>;
          print(results['Address']);
        }
        res = true;
      } else {
        print('nothing');
        res = false;
      }
      return const Text('loading');
    }); */
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    if (authUser.currentUser == null) {
      print('user not there');
      authUser.signInAnonymously();
      print('user there ano ${authUser.currentUser}');
    } else {
      print('user already there ${authUser.currentUser}');
    }

    return Scaffold(
      backgroundColor: Colors.orange[100],
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: const Text(
                  "Welcome.",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.orange,
                    fontSize: 30,
                    letterSpacing: 1,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(height: size.height * 0.03),
              Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: const Text(
                  "LOGIN",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.orange,
                    fontSize: 20,
                    letterSpacing: 1,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.symmetric(horizontal: 40),
                child: TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  onChanged: (value) {
                    tempEmail = value;
                  },
                  decoration:
                      const InputDecoration(labelText: "Please enter email"),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.symmetric(horizontal: 40),
                child: TextFormField(
                  onChanged: (value) {
                    tempPass = value;
                  },
                  decoration:
                      const InputDecoration(labelText: "Please enter password"),
                  obscureText: true,
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                margin:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: GestureDetector(
                  onTap: () => {},
                  child: const Text(
                    "Forgot your password?",
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.black26,
                    ),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 5),
                child: ElevatedButton(
                  onPressed: () {
                    login(tempEmail, tempPass, context);
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.orange,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 50, vertical: 20),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                    textStyle: const TextStyle(color: Colors.white),
                  ),
                  child: const Text(
                    "LOGIN",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                margin:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: GestureDetector(
                  onTap: () => {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const RegisterPage()),
                    )
                  },
                  child: const Text(
                    "Don't have a Login details? Create them",
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.black26,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterState();
}

void _showToast(BuildContext context, String msg) {
  final scaffold = ScaffoldMessenger.of(context);
  scaffold.showSnackBar(
    SnackBar(
      backgroundColor: Colors.orange,
      content: Text(msg),
      action:
          SnackBarAction(label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
    ),
  );
}

class _RegisterState extends State<RegisterPage> {
  ImagePicker picker = ImagePicker();
  File? imgFile = File("");
  String tempPass = "", confPass = "";
  @override
  void initState() {
    super.initState();
  }

  void getImage(String fName) async {
    final pp =
        FirebaseStorage.instance.ref().child('Profile Pictures').child(fName);

    ud.link = await pp.getDownloadURL();
    if (ud.link == '') {
      ud.link =
          'https://support.hubstaff.com/wp-content/uploads/2019/08/good-pic.png';
    }

    print(ud.link);
  }

  void registerUser() async {
    print('ref done renaming');
    //changeFileNameOnlySync(ud.image!, ud.phone);
    /*  String ext = path.extension(ud.image!.path);
      String dir = path.dirname(ud.image!.path);
      String newName = path.join(dir, '${ud.phone}$ext');
      ud.image!.renameSync(newName); */
    //print('renamed ${basename(ud.image!.path)}');
    late String fn = 'No Image';
    if (ud.image != null) {
      fn = basename(ud.image!.path);
    }

    Reference? PPRef = profilePics.child('Profile Pictures/${ud.phone}_$fn');

    setState(() {
      //final PP = PPRef.child(ud.image!.path);

      FirebaseFirestore.instance
          .collection('Users')
          .doc(ud.email)
          .set({
            'Full Name': ud.name,
            'Email': ud.email,
            'Phone': ud.phone,
            'Address': ud.address,
            'Password': ud.password,
            'Image': '${ud.phone}_$fn',
            'Link': ud.link
          })
          .then((value) => log('User added ${ud.name}'))
          .catchError(
              (onError) => log('Error adding user ${ud.name}: $onError'));
    });
    try {
      print('file wait');
      if (ud.image != null) {
        await PPRef.putFile(ud.image!);
        print('file done');
      }
    } on FirebaseException catch (e) {
      print(e);
    }
  }

  // ignore: non_constant_identifier_names
  Future<bool> user_exists(String email) {
    return FirebaseFirestore.instance
        .collection('Users')
        .doc(email)
        .get()
        .then((value) {
      return value.exists;
    });
  }

  @override
  Widget build(BuildContext context) {
    // CollectionReference users = FirebaseFirestore.instance.collection('Users');
/*     File changeFileNameOnlySync(File file, String newFileName) {
      var path = file.path;
      var lastSeparator = path.lastIndexOf(Platform.pathSeparator);
      var newPath = path.substring(0, lastSeparator + 1) + newFileName;
      return file.renameSync(newPath);
    } */

    // ignore: non_constant_identifier_names

    Future getIma() async {
      await picker.pickImage(source: ImageSource.gallery).then((value) {
        setState(() {
          ud.image = File(value!.path);
        });
      });

      setState(() {});
    }

    return Scaffold(
      backgroundColor: Colors.orange[100],
      body: SingleChildScrollView(
        child: SafeArea(
          minimum: const EdgeInsets.only(top: 10),
          child: Column(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  getIma();
                },
                child: CircleAvatar(
                    backgroundColor: Colors.orange[200],
                    radius: 50,
                    backgroundImage: ud.image != null
                        ? FileImage(ud.image!)
                        : NetworkImage(ud.link) as ImageProvider),
              ),
              const SizedBox(
                height: 20,
                width: 50,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 50,
                        child: Icon(Icons.person),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: 250,
                      child: TextFormField(
                        initialValue: ud.name,
                        decoration:
                            const InputDecoration(labelText: "Enter Full Name"),
                        onChanged: (name) {
                          ud.setName(name);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 50,
                        child: Icon(Icons.email),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: 250,
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        initialValue: ud.email,
                        decoration:
                            const InputDecoration(labelText: "Enter Email"),
                        onChanged: (email) {
                          ud.setEmail(email);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 50,
                        child: Icon(Icons.phone),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: 250,
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        initialValue: ud.phone,
                        decoration: const InputDecoration(
                            labelText: "Enter Phone Number"),
                        onChanged: (phone) {
                          ud.setPhone(phone);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 100,
                        child: Icon(Icons.location_city),
                      ),
                    ),
                    SizedBox(
                      height: 80,
                      width: 250,
                      child: TextFormField(
                        keyboardType: TextInputType.multiline,
                        maxLines: 10,
                        initialValue: ud.address,
                        decoration:
                            const InputDecoration(labelText: "Enter Address"),
                        onChanged: (address) {
                          ud.setAddress(address);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Card(
                child: Row(
                  children: [
                    const InkWell(
                      splashColor: Colors.grey,
                      child: SizedBox(
                        width: 70,
                        height: 50,
                        child: Icon(Icons.password),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                      width: 120,
                      child: TextFormField(
                        initialValue: ud.password,
                        decoration:
                            const InputDecoration(labelText: "Enter Password"),
                        onChanged: (password) {
                          tempPass = password;
                        },
                        obscureText: true,
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    SizedBox(
                      height: 50,
                      width: 140,
                      child: TextFormField(
                        initialValue: ud.password,
                        decoration: const InputDecoration(
                            labelText: "Confirm Password"),
                        onChanged: (password) {
                          confPass = password;
                        },
                        obscureText: true,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
                width: 300,
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 5),
                child: ElevatedButton(
                  onPressed: () {
                    if (tempPass != '' &&
                        confPass != '' &&
                        ud.address != '' &&
                        ud.email != '' &&
                        ud.name != '' &&
                        ud.phone != '') {
                      if (tempPass.compareTo(confPass) == 0) {
                        ud.setPassword(tempPass);
                        user_exists(ud.email).then((value) {
                          if (value == true) {
                            _showToast(context,
                                'User with the email already exists. Please login or use another email address');
                          } else {
                            registerUser();
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const LoginPage()),
                            );
                            _showToast(context, 'Registered. Please Log In');
                            ud = UserDetails();
                          }
                        });
                      } else {
                        _showToast(context, 'Passwords do not match');
                      }
                    } else {
                      _showToast(context, 'All fields are required');
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.orange,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 50, vertical: 20),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                    textStyle: const TextStyle(color: Colors.white),
                  ),
                  child: const Text(
                    "REGISTER",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(
                alignment: const Alignment(1.7, 0),
                margin:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: GestureDetector(
                  onTap: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const LoginPage())),
                  },
                  child: const Text(
                    "Already have an account? Login",
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.black26,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
